import time, json, httplib
import os, sys, select
from threading import Thread
import pygame
from pygame.locals import *

room_ids = { 'kitchen0-0'    : 'y0D6ft55L9',
             'bedroom0-0'    : '43gv5UrzsK',
             'bedroom0-1'    : '62fQrof0u8',
             'livingroom0-0' : 'sbozvvasUF' }

light_states = { 'kitchen0-0'    : False,
                 'bedroom0-0'    : False,
                 'bedroom0-1'    : False,
                 'bedroom1-0'    : False,
                 'livingroom0-0' : False }

room_pins = { 'kitchen0-0'    : '0',
              'bedroom0-0'    : '1',
              'bedroom0-1'    : '2',
              'bedroom1-0'    : '3',
              'livingroom0-0' : '4' }

room_digits = {  '0': 'kitchen0-0',
                 '1': 'bedroom0-0',
                 '2': 'bedroom0-1',
                 '3': 'bedroom1-0',
                 '4': 'livingroom0-0' }



pygame.init()


states_dirty = False

connection = httplib.HTTPSConnection('api.parse.com', 443)
connection.connect()

for pin in room_pins.itervalues():
    command = 'gpio mode ' + pin + ' out'
    os.system(command)


def state2str(state):
    return ('1' if state else '0')


def change_light(room, state):
    print 'Room: ' + room
    print room_pins
    command = 'gpio write ' + room_pins[room] + ' ' + state2str(state)
    os.system(command)


def light_changed(room, state):
    global states_dirty
    states_dirty = True
    light_states[room] = state
    res = update_light(room, state)
    states_dirty = False


def get_state():    
    connection.request('GET', '/1/classes/TestObject', '', {
       "X-Parse-Application-Id": "F5HHjgw5rAc9yVv3XmGzABDXkbKcZ7s7ajFXNVtq",
       "X-Parse-REST-API-Key": "mdQxQxin5DZS2iFLlpse8vjKMdOcXHRwuM7Ib8iX"})
    result = json.loads(connection.getresponse().read())
    return result


def update_light(roomName, state):
    global connection
    connection.request('PUT', '/1/classes/TestObject/' + room_ids[roomName], json.dumps({
         "isOn": state
       }), {
         "X-Parse-Application-Id": "F5HHjgw5rAc9yVv3XmGzABDXkbKcZ7s7ajFXNVtq",
         "X-Parse-REST-API-Key": "mdQxQxin5DZS2iFLlpse8vjKMdOcXHRwuM7Ib8iX",
         "Content-Type": "application/json"
       })

    result = json.loads(connection.getresponse().read())
    return result


def update_light_states():
    state = get_state()
    results = state[u'results']
    print "ROOM STATES:"
    for r in results:
        room = r[u'room']
        is_on = r[u'isOn']
        object_id = r[u'objectId']
        
        room_ids[room] = object_id
        light_states[room] = is_on
        change_light(room, is_on)

        print "Room: %s, isOn: %s" % (r[u'room'], r[u'isOn'])


iterations = int(raw_input("How many interations should we perform?  "))


for i in xrange(iterations):
    while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
        line = sys.stdin.readline()
        for letter in line.strip():
            room = room_digits[letter]
            state = not light_states[room]
            light_changed(room, state)
    else:    
        if states_dirty:
            time.sleep(1)

        update_light_states()
        time.sleep(3)


#print "Updating the kitchen light:"
#update_light('kitchen0-0', False)

#print "Updating light states: "
#update_light_states()


