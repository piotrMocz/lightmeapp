//
//  DetailViewController.swift
//  ParseStarterProject
//
//  Created by Michał on 05/05/15.
//  Copyright (c) 2015 Parse. All rights reserved.
//

import UIKit
import Parse
import ParseUI


class DetailViewController: UIViewController {

    var currentObject : PFObject?
    
    @IBOutlet weak var mySwitch: UISwitch!
    
    @IBAction func saveClicked(sender: AnyObject) {
        if let obj = currentObject{
            obj["isOn"] = mySwitch.on
            obj.saveEventually(nil)
        }
        
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let object = currentObject{
            let state = currentObject!["isOn"] as? Bool
            mySwitch.setOn(state!, animated: false)
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
